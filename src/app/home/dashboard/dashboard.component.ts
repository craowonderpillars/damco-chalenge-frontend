/// <reference types="@types/googlemaps" />
import { Component, OnInit } from '@angular/core';
import { HomeService } from '../../services';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  title = 'My first AGM project';
  lat = 33.3881;
  long = -112.8617;
  markers = [{
    lat: 33.3881,
    lng: -112.8617,
    label: 'Maricopa'
  }];
  tempMarkers: Array<any> = [];
  isLoading: Boolean = true;
  filterArr: Array<any> = [];
  constructor(
    private homeService: HomeService,
  ) { }

  ngOnInit(): void {
    this.getRecords();
  }
  /** Get Records form api 
   * Display these records on google map
   */
  getRecords() {
    this.homeService.getAllCompanies().subscribe((response: any) => {
      let { data, filterArr } = response;
      this.markers = data;
      this.tempMarkers = data;
      this.filterArr = filterArr;
      this.isLoading = false;
    })
  }
  /** Get records from original records based on selected option
   * Assign this value on marker array which are display on google map
   */
  onOptionsSelected($event: any) {
    let markerList = [...this.tempMarkers];
    if ($event.target.value != 'all') {
      markerList = markerList.filter(obj => obj.state == $event.target.value)
      this.markers = markerList;
    }
    else {
      this.markers = markerList;
    }

  }
}
