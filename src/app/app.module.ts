import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { HomeService, JWTInterceptor } from './services';
import { ShareModule } from './share/share.module';
import { AgmCoreModule, GoogleMapsAPIWrapper } from '@agm/core';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ShareModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDMqBb2hnvEcMt6KTYqGx2YPZaWOPn6O4E', //Add your own API key
      libraries: ['places']
    })
  ],
  providers: [
    HomeService,
    GoogleMapsAPIWrapper,
    { provide: HTTP_INTERCEPTORS, useClass: JWTInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
