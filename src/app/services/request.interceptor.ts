import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/do';
import { throwError } from 'rxjs';
import { catchError } from "rxjs/operators";
import { Router } from '@angular/router';

import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpErrorResponse
} from '@angular/common/http';

import { environment } from '../../environments/environment';

@Injectable()
export class JWTInterceptor implements HttpInterceptor {
    public API_ENDPOINT = environment.apiUrl + 'api/';

    constructor(
        private router: Router,
    ) {
    }

    /**
     * Take any HTTP request made with HttpClient and add the proper outgoing headers for authentication purposes.
     * @param req
     * @param next
     * @returns {Observable<HttpEvent<any>>}
     */
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // modify the request
        var headerRequest = {
            headers: req.headers
                .set('Content-Type', 'application/json')
                .set('Cache-Control', 'no-cache')
                .set('Pragma', 'no-cache')
        };
        const authReq = req.clone(headerRequest);
        // send to the next interceptor or to its final destination
        return next.handle(authReq).pipe(
            catchError((error: HttpErrorResponse) => {
                if (error.status !== 401) {
                    // 401 handled in auth.interceptor
                    // this.toastr.error(error.message);
                }
                return throwError(error);
            })
        );
    }
}
