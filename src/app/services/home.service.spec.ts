import { HttpClient } from '@angular/common/http';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { defer } from 'rxjs';
import { HomeService } from './home.service';

describe('Home Service', () => {
    let homeService: HomeService;
    let httpClientSpy: { get: jasmine.Spy };
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                HttpClient,
                HomeService,
                { provide: HttpClient, useValue: httpClientSpy },
            ],
        });
        homeService = TestBed.inject(HomeService);
    });

    it('is created', () => {
        expect(homeService).toBeDefined();
    });

    it('call getAllCompanies()', fakeAsync(() => {
        const testData = [
            {
                lat: 33.3881,
                lng: -112.8617,
                label: 'Maricopa'
            }
        ];

        httpClientSpy.get.and.returnValue(defer(() => Promise.resolve(testData)));

        homeService.getAllCompanies().subscribe((data) => {
            expect(data).toEqual(testData);
        });
        tick();
    }));

});