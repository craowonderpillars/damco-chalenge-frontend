# damco-chalenge-frontend

## Getting started

# Given:
We have list of states and each state have list of top five companies belongs to that state.

# Purpose:
1. By default We have to show all these companies on Google Map.
2. Displaying the dropdown list of state
3. When we select state from the dropdown list after that displaying compnies on google map based on selected state.

# Prerequisite for project:
1. Angular CLI should be install in your machine and
2. Node JS (14+ Version)

# Check angular version

ng version
     _                      _                 ____ _     ___
    / \   _ __   __ _ _   _| | __ _ _ __     / ___| |   |_ _|
   / △ \ | '_ \ / _` | | | | |/ _` | '__|   | |   | |    | |
  / ___ \| | | | (_| | |_| | | (_| | |      | |___| |___ | |
 /_/   \_\_| |_|\__, |\__,_|_|\__,_|_|       \____|_____|___|
                |___/
    

Angular CLI: 14.1.0
Node: 14.18.0
Package Manager: npm 6.14.15 
OS: linux x64

Angular: 
... 

Package                      Version
------------------------------------------------------
@angular-devkit/architect    0.1401.0 (cli-only)
@angular-devkit/core         14.1.0 (cli-only)
@angular-devkit/schematics   14.1.0 (cli-only)
@schematics/angular          14.1.0 (cli-only)


# How to start
We will create new project with command "ng new <Project Name>" in terminal.
1. ng new <Project Name>
2. npm install rxjs
3. npm install rxjs-compat@^6.6.7

# Running the project

npm start

# Testing the project

Install the karma-jasmine

Run the below command for testing

npm install karma-jasmine